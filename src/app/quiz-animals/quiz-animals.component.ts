import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Question} from '../model/question.model';

@Component({
  selector: 'app-quiz-animals',
  templateUrl: './quiz-animals.component.html',
  styleUrls: ['./quiz-animals.component.css']
})
export class QuizAnimalsComponent implements OnInit {
  indexQuestion = 0;
  questions: Question[] = [];
  total = 0;
  completed = false;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get('./assets/data.json').subscribe((questions: Question[]) => {
      this.questions = questions;
    });
  }

  onSelectItem(selectedAnswer, arrAnswers) {
    arrAnswers.forEach((answer) => {
      if (answer.answer === selectedAnswer.answer) {
        answer.checked = true;
      } else {
        answer.checked = false;
      }
    });
  }

  onPrevious() {
    this.indexQuestion--;
  }

  onNext() {
    this.indexQuestion++;
  }

  showResult() {
    this.total = 0;
    this.completed = true;
    this.questions.forEach((question) => {
      question.answers.forEach((answer) => {
        if (answer.checked) {
          this.total += answer.mark;
        }
      });
    });
  }
}
