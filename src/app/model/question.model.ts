export class Question {
  question: 'string';
  answers: [{
    answer: 'string',
    mark: number,
    checked: boolean
}];
}
